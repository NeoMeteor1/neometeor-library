<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Squaretown</title>
    {!! HTML::style('assets/neometeor.css') !!}
</head>
<body>
<nav>
    <div>
        <div>

            <!-- Collapsed Hamburger -->
            <button>
                <span>Toggle Navigation</span>
                <span></span>
                <span></span>
                <span></span>
            </button>

            <!-- Branding Image -->
            {{ \Neometeor\Library\NeoLibrary::link('Squaretown', '/', array("class" => "neologo")) }}
        </div>

        <div>
            <!-- Left Side Of Navbar -->
            <ul>
                <li><a href="{{ url('/districts') }}">Home</a>{{ \Neometeor\Library\NeoLibrary::link('Home', '/') }}</li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul>
                <li>
                    <ul>
                        <li>{{ \Neometeor\Library\NeoLibrary::link('Logout', '/logout', array("class" => "italic")) }}</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div>
    @yield('content')
</div>

<!-- JavaScripts -->
{!! HTML::script('assets/jquery-2.1.4.js') !!}
{!! HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js') !!}
</body>
</html>